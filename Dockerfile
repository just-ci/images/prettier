FROM node:alpine

RUN npm install -g prettier && npm cache clear --force
